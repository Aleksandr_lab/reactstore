import React, {Fragment,Component} from 'react';
import {Button, Form} from "react-bootstrap";

import {classNames} from "../helpers/Helpers";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {faStar as faRegStar} from "@fortawesome/free-regular-svg-icons";
import {Error} from "../atom/Error";
import axios from "axios";


class  FormReviews extends Component{
    constructor(props) {
        super(props);
        this.state = {
            indexHoverStars: -1,
            selectedStars: new Map(),
        };
    }
    handleSubmit = e => {
        e.preventDefault();
        const {selectedStars} = this.state,
            {isLoggedIn,product, apiUrl,nextReviews,redirect, resError, logOut} = this.props,
            rate = selectedStars.get(product.id)+1;
        if (!isLoggedIn){
            setTimeout(()=>redirect('/authenticated'),1);
            return resError('You should sign in first');
        }
        if(!rate){
            return resError('Please indicate rating');
        }
        const data = {
            rate,
            "text":e.target.message.value
        };
        const headers = {
            "Authorization": ` Token ${isLoggedIn.token}`
        };
        axios
            .get(apiUrl + 'api/reviews/' + product.id, {})
            .then(res => nextReviews(res.data));
        axios
            .post(apiUrl + 'api/reviews/' + product.id, data, {headers})
            .then(res => {
            }).catch(()=>{
            redirect('/authenticated');
            resError('Please re-create account');
            logOut();
        });
    };
    stateHoverIndexStart = (index) => {
        this.setState({
            ...this.state,
            indexHoverStars:index
        })
    };
    stateSelectedStarted = (index) => {
        this.state.selectedStars.set(this.props.product.id, index);
        this.props.resError(false)
    };

    clearHoverStars = () => {
        this.setState({
            ...this.state,
            indexHoverStars:-1,
        })
    };

    render (){
        const {product} = this.props;
        const {indexHoverStars,selectedStars} = this.state;
        return(
            <Fragment>
                <Form onSubmit={this.handleSubmit} className={'mb-3'}>
                    <Form.Group className={'form-rating'} onMouseLeave={()=>  this.clearHoverStars()}>
                        {
                            [...Array(5)].map((el,i) => {
                                let classIcon = classNames({
                                    'click-stars': selectedStars.get(product.id)  >= i,
                                    'hover-stars': indexHoverStars >= i
                                });
                                return (
                                    <FontAwesomeIcon
                                        key={i}
                                        onMouseOver={
                                            ()=> this.stateHoverIndexStart(i)
                                        }
                                        onClick={
                                            ()=> this.stateSelectedStarted(i)
                                        }
                                        className={classIcon}
                                        icon={
                                            indexHoverStars >= i || selectedStars.get(product.id)  >= i
                                                ?
                                                faStar
                                                :faRegStar }
                                    />
                                )
                            })
                        }
                        <span className={'ml-3'}><i>Stars: {indexHoverStars+1 || selectedStars.get(product.id)+1 || 0}</i></span>
                    </Form.Group>
                    <Form.Group>
                        <Form.Control as="textarea" placeholder={"Type your review..."} name={'message'} rows="3" required/>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Submit Review
                    </Button>
                </Form>
            </Fragment>
        )
    };
}
export {FormReviews}