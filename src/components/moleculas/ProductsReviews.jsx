import React, {Component, Fragment} from 'react';
import {ReviewsList} from "./ReviewsList";
import {FormReviews} from "./FormReviews";

class ProductReviews extends Component{
    constructor(props) {
        super(props);
        this.getReviews = this.getReviews.bind(this);
        this.state = {
            nextReviews: null
        };
    }
    getReviews(val){
        this.setState({
            nextReviews:val
        });
    }
    render () {
        const {apiUrl, reviews, product, isLoggedIn, redirect, resError,logOut} = this.props;
        const {nextReviews} = this.state;
        return (
            <Fragment>
                <h2>Reviews</h2>

                <FormReviews logOut={logOut}  resError={resError} redirect={redirect} nextReviews={this.getReviews} apiUrl={apiUrl} isLoggedIn={isLoggedIn} product={product} className="form-reviews mb-2"/>

                <ReviewsList className="list-reviews" reviews={nextReviews?nextReviews:reviews}/>

            </Fragment>
        )
    }
};

export {ProductReviews};