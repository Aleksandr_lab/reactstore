import React, {Fragment} from 'react';
import {Button, Form} from "react-bootstrap";

const UserLogin = ({getUsersForm}) => {
    return (
        <Fragment>
            <Form id={'formLogin'} onSubmit={getUsersForm}>
                <Form.Group controlId="formLoginName">
                    <Form.Label>Username:</Form.Label>
                    <Form.Control required type="text" name={"username"} />
                </Form.Group>
                <Form.Group controlId="formLoginPassword">
                    <Form.Label>Password:</Form.Label>
                    <Form.Control required type="password" name={"password"} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Login
                </Button>
            </Form>
        </Fragment>
    );
};
export {UserLogin}