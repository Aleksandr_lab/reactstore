import React, {Fragment} from 'react';
import {Image} from "react-bootstrap";

const ProductInfo = ({product, apiUrl}) => {
    return (
        <Fragment>
            <h1 className="text-uppercase">{product.title}</h1>
            <div className="mb-2">
                <Image src={`${apiUrl}static/${product.img}`} rounded className="border"/>
            </div>
            <div>
                <h3>Product Description</h3>
                <p>{product.text}</p>
            </div>
        </Fragment>
    )
};

export {ProductInfo};