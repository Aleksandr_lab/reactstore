import React, {Fragment} from 'react';
import {Button, Form} from "react-bootstrap";

const UserRegistration = ({getUsersForm}) => {
    return (
        <Fragment>
            <Form id={'formRegister'} onSubmit={getUsersForm}>
                <Form.Group controlId="formRegisterName">
                    <Form.Label>Username:</Form.Label>
                    <Form.Control required type="text" name={"username"} />
                </Form.Group>
                <Form.Group controlId="formRegisterPassword1">
                    <Form.Label>Enter Password:</Form.Label>
                    <Form.Control required type="password" name={"password1"} />
                </Form.Group>
                <Form.Group controlId="formRegisterPassword2">
                    <Form.Label>Repeat Password:</Form.Label>
                    <Form.Control required type="password" name={"password2"} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Register
                </Button>
            </Form>
        </Fragment>
    );
};
export {UserRegistration}