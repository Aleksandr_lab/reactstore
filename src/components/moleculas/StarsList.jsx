import React, {Fragment} from 'react';
import {Star} from "../atom/Star";

const StarsList = ({item}) => {
    return (
        <Fragment>
            {
                [...Array(5)].map((el,i) => <Star key={i} item={item} index={i}/> )
            }
        </Fragment>
    );
};
export {StarsList}