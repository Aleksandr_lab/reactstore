import React, {Fragment} from 'react';

import moment from "moment";

import {Card} from "react-bootstrap";
import {StarsList} from "./StarsList";


const ReviewsList = ({reviews}) => {
    return (
        <Fragment>
            {
                reviews.sort((a,b)=> new Date(b.created_at) - new Date(a.created_at))
                    .map(review=>{
                    return(
                        <Card key={review.id} className="mb-2">
                            <Card.Body>
                                <Card.Title>Author: <i><b>{review.created_by.username}</b></i> {moment(review.created_at).format('DD MMMM, YYYY, HH:mm')}</Card.Title>
                                <Card.Text>
                                    <StarsList item={review.rate}/>
                                </Card.Text>
                                <Card.Text>
                                    {review.text}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </Fragment>
    )
};

export {ReviewsList};