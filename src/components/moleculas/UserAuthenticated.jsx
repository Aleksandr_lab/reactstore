import React, {Component, Fragment} from 'react';
import axios from "axios";

import {Row, Col} from "react-bootstrap";

import {UserLogin} from "./UserLogin";
import {UserRegistration} from "./UserRegistration";
import {Error} from "../atom/Error";


class UserAuthenticated extends Component {
    constructor(props) {
        super(props);
    }

    getUsersForm = e => {
        e.preventDefault();
        const {resError, createUser, apiUrl, redirect} = this.props;
        let api = 'api/login/',
            userValue =  Object.fromEntries(new FormData(e.target).entries());
        if(e.target.id === 'formRegister'){
            api =  'api/register/';
            if(userValue.password1 === userValue.password2){
                userValue.password = userValue.password1;
            }else{
                resError('Passwords do not match');
                return null
            }
        }
        axios
            .post(apiUrl + api,userValue)
                .then(res => res.data)
                    .then(data => {
                        if(data.success){
                            const user =  {
                                success: data.success,
                                token: data.token,
                                userName: userValue.username
                            };
                            resError(null);
                            localStorage.setItem('User', JSON.stringify(user));
                            createUser(user);
                            redirect('/product/1');
                }else{
                    resError(data.message)
                }
            })
    };

    render() {
        return (
            <Fragment>
                <Row>
                    <Col>
                        <h3>User Login</h3>
                        <UserLogin getUsersForm={e=>this.getUsersForm(e)}/>
                    </Col>
                    <Col>
                        <h3>Registration users</h3>
                        <UserRegistration getUsersForm={e=>this.getUsersForm(e)}/>
                    </Col>
                </Row>

            </Fragment>
        )
    }
}

export {UserAuthenticated};