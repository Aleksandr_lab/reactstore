import React, {Fragment} from 'react';
import {DropdownButton,Dropdown} from "react-bootstrap";

const UserHeader = ({user, logOut}) => {
    return (
        <Fragment>
            <DropdownButton id="dropdown-basic-button" title={user.userName}>
                <Dropdown.Item onClick={logOut}>Log out</Dropdown.Item>
            </DropdownButton>
        </Fragment>
    );
};
export {UserHeader}