import React, {Fragment} from 'react';

import Page from './organism/Page'

const App = () => {
  return (
      <Fragment>
        <Page />
      </Fragment>
  );
};

export default App;
