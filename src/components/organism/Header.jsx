import React, {Fragment } from 'react';
import {Navbar, Nav, Col, DropdownButton,} from 'react-bootstrap';
import Container from "react-bootstrap/Container";

import {Link} from "react-router-dom";
import {UserHeader} from "../moleculas/UserHeader";


const Header = ({isLoggedIn,products, resError,logOut}) => {
    let {...success}= isLoggedIn;
    if(Object.keys(success).length === 0){
        success = false;
    }
    return (
        <Fragment>
            <Navbar bg="dark" expand="lg" variant="dark" sticky="top" className="mb-3">
                <Container>
                    <Link className={'navbar-brand'} to="/">Store</Link>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto" >
                            {
                                products.map(el => {
                                    return (
                                        <Nav.Item key={el.id}>
                                            <Link
                                                onClick={()=>resError(null)}
                                                to={"/product/"+el.id}
                                                className={'nav-link'}
                                            >
                                                {el.title}
                                            </Link >
                                        </Nav.Item>
                                    )
                                })

                            }
                            { !success
                                ? <Nav.Item>
                                            <Link
                                                onClick={()=>resError(null)}
                                                to="/authenticated"
                                                className={'nav-link'}
                                            >
                                                Login
                                            </Link >
                                        </Nav.Item>
                                    : null
                            }
                        </Nav>
                    </Navbar.Collapse>
                    {isLoggedIn?<UserHeader logOut={logOut} user={isLoggedIn}/>:''}
                </Container>
            </Navbar>
        </Fragment>
    )
};

export  {Header};
