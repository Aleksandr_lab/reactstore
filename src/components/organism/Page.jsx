import React, { Component } from 'react';
import axios from "axios";

import {Product} from './Product';
import {Header} from "./Header";

import { Container, Row} from "react-bootstrap";

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom"
import {UserAuthenticated} from "../moleculas/UserAuthenticated";
import {Error} from "../atom/Error";


class Page extends Component {
    constructor(props){
        super(props);
        this.newRedirect = this.newRedirect.bind(this);
        this.getError = this.getError.bind(this);
        this.logOut = this.logOut.bind(this);
        this.createUser = this.createUser.bind(this);
        this.state = {
            products: [],
            apiUrl: 'http://smktesting.herokuapp.com/',
            selectedProduct: null,
            reviews: [],
            nextReviews: true,
            isLoggedIn: false,
            redirectGo: null,
            resError: null
        }
    }

    componentDidMount(){
        axios
            .get(this.state.apiUrl + 'api/products/',{})
           .then(res => {
                this.setState({
                    ...this.state,
                    products: res.data
                });
            });
    }

    componentDidUpdate(){
        if(localStorage.getItem('User')){
            this.state.isLoggedIn= JSON.parse(localStorage.getItem('User'));
        }
    }

    selectedProduct(products, routId){
        const {apiUrl, nextReviews} = this.state;
        axios
            .get(apiUrl + 'api/reviews/' + routId, {})
            .then(res => {
                this.setState(prevStet => {
                    if(nextReviews){
                        return ({
                            reviews: res.data,
                            nextReviews: false
                        })
                    }else{
                        this.state.nextReviews = true
                    }
                })
            });

        return(
            products.filter(el => el.id == routId)
        );
    }
    newRedirect(val){
        this.setState({
            ...this.state,
            redirectGo: val
        });
    }
    getError(val){
        this.setState({
            ...this.state,
            resError: val,
            redirectGo: null
        });
    }
    logOut(){
        localStorage.removeItem('User');
        this.setState({
            ...this.state,
            isLoggedIn: false
        });
    }

    createUser(val){
        this.setState({
            ...this.state,
            isLoggedIn: val
        });
    }

    render() {

        const {isLoggedIn,products,selectedProduct,apiUrl,reviews,redirectGo,resError} = this.state;

        return (
            <Router>
                <Header logOut={this.logOut} resError={this.getError} isLoggedIn={isLoggedIn} selectedProduct={selectedProduct} products={products}/>
                <Container>
                    {redirectGo?<Redirect to={redirectGo}/> :''}
                    {   resError ?
                        <Error errorMessage={resError}/>
                        : null  }
                    <Route exact path={"/"} render = {()=> 'Not selected Products'}/>
                    <Row>
                        <Route path={["/product/:id"]} render = {el=> {
                            const productId =  el.match.params.id;
                            return  (
                                <Product resError={this.getError} logOut={this.logOut} redirect={this.newRedirect} isLoggedIn={isLoggedIn} reviews={reviews} id={productId} product={this.selectedProduct(products,productId)[0]} apiUrl={apiUrl}/>
                            )
                        }}/>
                    </Row>

                    <Route  path={["/authenticated"]} render = {()=> <UserAuthenticated createUser={this.createUser}  resError={this.getError} redirect={this.newRedirect} apiUrl={apiUrl}/> }/>
                </Container>
            </Router>
        )
    }
}

export  default  Page;