import React, {Fragment} from 'react';

import {ProductInfo} from "../moleculas/ProductInfo";
import {ProductReviews} from "../moleculas/ProductsReviews";

import {Col} from "react-bootstrap";


const Product = ({apiUrl, product, reviews,isLoggedIn, redirect, resError, logOut}) =>{
    const {...productProps} = product;
    return (
        <Fragment>
            <Col md={6}>
                <ProductInfo product={productProps} apiUrl={apiUrl}/>
            </Col>
            <Col md={6}>
                <ProductReviews logOut={logOut} resError={resError} redirect={redirect} isLoggedIn={isLoggedIn} apiUrl={apiUrl} product={productProps} reviews={reviews}/>
            </Col>
        </Fragment>
    )
};

export {Product};