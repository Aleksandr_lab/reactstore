import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as faRegStar } from '@fortawesome/free-regular-svg-icons'

const Star = ({index, item}) => {
    return (
           <FontAwesomeIcon icon={index <= item ? faStar : faRegStar}/>
    );
};
export {Star}