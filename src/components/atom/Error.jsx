import React, {Fragment} from 'react';
import {Alert} from "react-bootstrap";

const Error = ({errorMessage}) => {
    return (
        <Fragment>
            <Alert variant="danger">
                <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                <p>
                    {errorMessage}
                </p>
            </Alert>
        </Fragment>
    );
};
export {Error}