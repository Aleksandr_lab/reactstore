const classNames = (classes) => {
        return Object.entries(classes)
            .filter(([key, value]) => value)
            .map(([key]) => key)
            .join(' ');
};

export {classNames};